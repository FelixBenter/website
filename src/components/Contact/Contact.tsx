import React from 'react';
import { motion } from 'framer-motion';
import InView from 'react-intersection-observer';

import styles from './Contact.module.scss';

import { gitlabIcon } from '../../util/images';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faLinkedin } from '@fortawesome/free-brands-svg-icons';

function Contact() {

    type Props = {
        children? : React.ReactNode
        inView? : boolean
    }

    const MotionContainer = React.forwardRef<any, Props>((props: Props, ref) => (
        // @ts-ignore
        <div ref={ref}>{props.children}</div>
    ))

    const list = {
        visible: { 
            opacity: 1,
            transition: {
                when: "beforeChildren",
                staggerChildren: 0.5,
            },
         },
        hidden: { 
            opacity: 0,
            transition: {
                when: "afterChildren",
            }
        },
      }

    const item = {
    visible: { opacity: 1, rotate: 360 },
    hidden: { opacity: 0, rotate: 0 },
    }

    return(
        <div className="content-container">
            <div className="content">
                <h2>Contact Me</h2>
                <InView>
                    {({ inView, ref }) => (
                        <MotionContainer ref={ref}>
                            <motion.div className={styles.social} initial="hidden" animate={inView ? "visible" : "hidden"} variants={list}>
                                <motion.div variants={item} whileHover={{ scale: 1.2 }} whileTap={{ scale: 0.9 }}>
                                    <a href="mailto:contact@patrickbenter.com" aria-label="Email me here"><FontAwesomeIcon icon={faEnvelope} size="3x" color="white"/></a>
                                </motion.div>

                                <motion.div variants={item} whileHover={{ scale: 1.2 }} whileTap={{ scale: 0.9 }}>
                                    <a href="https://gitlab.com/dopfer" aria-label="Find me on GitLab" target="_blank" rel="noopener noreferrer"><img src={gitlabIcon} alt="Gitlab Icon" width="48" height="48"/></a>
                                </motion.div>

                                <motion.div variants={item} whileHover={{ scale: 1.2 }} whileTap={{ scale: 0.9 }}>
                                    <a href="https://www.linkedin.com/in/patrick-benter/" aria-label="Find me on LinkedIn" target="_blank" rel="noopener noreferrer"><FontAwesomeIcon icon={faLinkedin} size="3x" color="#0072b1"/></a>
                                </motion.div>
                            </motion.div>
                        </MotionContainer>
                    )}
                </InView>
            </div>
        </div>
    )
}

export { Contact };
