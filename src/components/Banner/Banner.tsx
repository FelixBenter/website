import React, { useEffect, useRef, useState } from 'react';
import { Modal } from 'react-bootstrap';
import rellax from 'rellax';
import Rellax from 'rellax';

import styles from './Banner.module.scss';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faImages } from '@fortawesome/free-solid-svg-icons';
import { parallax } from '../../util/images';

// @ts-ignore
const Parallax = ({ children, as: Component = "div", ...props}) => {
    const rellaxRef = useRef(null);
    useEffect(() => {
        // @ts-ignore
        let rellax = new Rellax(rellaxRef.current);
        return () => {
            rellax.destroy();
        };
    }, []);

    return (
        // @ts-ignore
        <Component ref={rellaxRef} {...props}>
            {children}
        </Component>
    )
}

function Banner() {

    const [state, setState] = useState({
        open: false
    });
    function openModal() {
        setState({ open: true })
    };
    function closeModal() {
        setState({ open: false })
    };

    function isMobile(speed: string) {
        return window.innerWidth < 720 ? "0" : speed;
    };

    const parallaxLayers = parallax.map((layer, index) => 
        <Parallax as="div" key={index} className={[styles.parallax__layer, rellax].join(' ')} data-rellax-speed={isMobile(layer.parallaxSpeed)} data-rellax-zindex={layer.parallaxZIndex}>
            <div style={{ backgroundImage: "url(" + layer.src + ")", height: "100%"}}></div>
        </Parallax>
    );

    return (
        <div className={styles.parallax}>
 
            {parallaxLayers}

            <Parallax as="div" className={[styles.parallax__layer, rellax].join(' ')} data-rellax-speed={isMobile("1")}>
                <div className={styles.title}><h1>Patrick Benter</h1></div>
            </Parallax>
            
            <div className={styles.attribution}>
                <FontAwesomeIcon icon={faImages} onClick={openModal} size="1x" />

                <Modal show={state.open} onHide={closeModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Mountain Background Vector</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <a href="https://www.freepik.com/free-photos-vectors/background">Background vector created by freepik - www.freepik.com</a>
                    </Modal.Body>
                </Modal>
            </div>
        </div>
    )
}

export { Banner };