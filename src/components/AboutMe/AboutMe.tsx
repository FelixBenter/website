import React from 'react';
import styles from './AboutMe.module.scss';

import { profile, profileSrcSet } from '../../util/images';

function AboutMe() {

    return(
        <div className="content-container">
            <div className="content">
            <h2>About Me</h2>
                <div className={styles.container}>
                    
                    <div className={styles.half}>
                        <p>
                            Hey, I'm Patrick. I'm currently studying at the ANU in Canberra, Australia. 
                            I'm a software developer, with experience primarily creating data pipelines
                            and visualizing data. 
                        </p>
                    </div>
                    <div className={styles.half}>
                        <img
                            className={styles.profileImage}
                            src={profile[profile.length - 1].src}
                            srcSet={profileSrcSet}
                            sizes={"(max-width: 720px) 60vw, 30vw"}
                            alt="Patrick Benter"
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}

export { AboutMe };