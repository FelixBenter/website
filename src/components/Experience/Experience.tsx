import React from 'react';
import { motion } from 'framer-motion';
import { InView } from 'react-intersection-observer';

import styles from './Experience.module.scss';

function Experience() {

    type Props = {
        children? : React.ReactNode
        inView? : boolean
    }

    const MotionContainer = React.forwardRef<any, Props>((props: Props, ref) => (
        // @ts-ignore
        <div ref={ref}>{props.children}</div>
    ))
    const textAnimTransition = { type: "spring", stiffness: 260, damping: 20 }

    const options = {
        threshold: 0.3,
        triggerOnce: true,
    }

    return(
        <div className="content-container">
            <div className="content">
                <h2>Experience</h2>
                
                <InView {...options}>
                    {({ inView, ref }) => (
                        <MotionContainer ref={ref}>
                            <motion.div ref = {ref} className={styles.fling} animate={{ opacity: inView ? 1 : 0, right: inView ? 0 : -10  }} transition={ textAnimTransition }>
                                <h3>Education</h3>
                                <div className={styles.row}>
                                    <div className={styles.columnHeading}>
                                        <p className={styles.job}><strong>Australian National University (ANU)</strong></p>
                                        <p className={styles.date}>2015-2020</p>
                                    </div>
                                    <div className={styles.column}>
                                        <p className={styles.workTitle}><strong>Bachelor of Software Engineering (Honours)</strong></p>
                                        <p>
                                            I am currently studying my fourth year, finishing July 2020. 
                                            The main skills I have gained are in frontend and backend systems, developing 
                                            web based user interfaces and using embedded devices to serve and capture data. 
                                        </p>
                                    </div>
                                </div>
                            </motion.div>
                        </MotionContainer>
                    )}
                </InView>

                <InView {...options}>
                    {({ inView, ref }) => (
                        <MotionContainer ref={ref}>
                            <motion.div ref = {ref} className={styles.fling} animate={{ opacity: inView ? 1 : 0, right: inView ? 0 : -10 }} transition={ textAnimTransition }>
                                <h3>Work</h3>
                                <div className={styles.row}>
                                    <div className={styles.columnHeading}>
                                        <p className={styles.job}><strong>ActewAGL</strong></p>
                                        <p className={styles.date}>2020 - Current</p>
                                    </div>
                                    <div className={styles.column}>
                                        <p className={styles.workTitle}><strong>Digital Intern</strong></p>
                                        <p>
                                            Created a data pipeline to read CSV files into a SQL Server 
                                            database using SSIS, then deployed interactive reports using SSRS. 
                                            This software improved productivity by automating a time 
                                            consuming manual process.
                                        </p>
                                    </div>
                                </div>
                                <div className={styles.row}>
                                    <div className={styles.columnHeading}>
                                        <p className={styles.job}><strong>Department of Primary Industries (DPI)</strong></p>
                                        <p className={styles.date}>2019</p>
                                    </div>
                                    <div className={styles.column}>
                                        <p className={styles.workTitle}><strong>Technical Life Cycle Assistant</strong></p>
                                        <p>
                                            Created an interactive graph using Python and Excel, to dynamically
                                            display crop data and respond to user input. This software read an 
                                            excel file of crop data and graphed the data in a web browser, 
                                            implementing various dropdowns and checkboxes which interfaced 
                                            live with the data.
                                        </p>
                                    </div>
                                </div>
                                <div className={styles.row}>
                                    <div className={styles.columnHeading}>
                                        <p className={styles.job}><strong>Envirowest Consulting</strong></p>
                                        <p className={styles.date}>2014</p>
                                    </div>
                                    <div className={styles.column}>
                                        <p className={styles.workTitle}><strong>Laboratory Assistant</strong></p>
                                        <p>
                                            Collected field samples of soil and performed 
                                            laboratory tests for optimal soil conditions. 
                                            These tests were undertaken with industry standards and 
                                            reported in a standardised format.
                                        </p>
                                    </div>
                                </div>
                            </motion.div>
                        </MotionContainer>
                    )}
                </InView>

                <InView {...options}>
                    {({ inView, ref }) => (
                        <MotionContainer ref={ref}>
                            <motion.div ref = {ref} className={styles.fling} animate={{ opacity: inView ? 1 : 0, right: inView ? 0 : -10 }} transition={ textAnimTransition }>
                                <h3>University Projects</h3>
                                <div className={styles.row}>
                                    <div className={styles.columnHeading}>
                                        <p className={styles.job}><strong>Farmbot Farm Designer</strong></p>
                                        <p className={styles.date}>2019</p>
                                    </div>
                                    <div className={styles.column}>
                                        <p className={styles.workTitle}><strong>Team Leader / Developer</strong></p>
                                        <p>
                                            I led a team for a year to design programs for designing 
                                            sustainable farms. Besides managing, I worked on software 
                                            to generate farm designs for a Farmbot machine, as well 
                                            as an Angular web interface to plan sustainable farms on a 
                                            larger scale, hosted on Github Pages. 
                                        </p>
                                    </div>
                                </div>
                                <div className={styles.row}>
                                    <div className={styles.columnHeading}>
                                        <p className={styles.job}><strong>Elementice</strong></p>
                                        <p className={styles.date}>2018</p>
                                    </div>
                                    <div className={styles.column}>
                                        <p className={styles.workTitle}><strong>Node.js Backend Developer</strong></p>
                                        <p>
                                            Worked with Elementice to develop a 360° camera rig to 
                                            capture still motion images. I developed 
                                            a NodeJS backend to coordinate image capture and video 
                                            streaming across multiple cameras, and a web front end 
                                            to trigger image capture and view the video stream.
                                        </p>
                                    </div>
                                </div>
                                <div className={styles.row}>
                                    <div className={styles.columnHeading}>
                                        <p className={styles.job}><strong>Clearbox Systems</strong></p>
                                        <p className={styles.date}>2018</p>
                                    </div>
                                    <div className={styles.column}>
                                        <p className={styles.workTitle}><strong>Typescript Frontend Developer</strong></p>
                                        <p>
                                        Worked with Clearbox Systems to create a web interface 
                                        to monitor a live radio spectrum simulating 
                                        radar data. I worked in the frontend team to develop 
                                        the web interface in Typescript.
                                        </p>
                                    </div>
                                </div>
                            </motion.div>
                        </MotionContainer>
                    )}
                </InView>
            </div>
        </div>
    )
}

export { Experience };