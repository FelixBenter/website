import React from 'react';
import { InView } from 'react-intersection-observer';
import { motion } from 'framer-motion';

import styles from './Skills.module.scss';

function Skills() {

    type Props = {
        children? : React.ReactNode
        inView? : boolean
    }

    const MotionContainer = React.forwardRef<any, Props>((props: Props, ref) => (
        // @ts-ignore
        <div ref={ref}>{props.children}</div>
    ))

    const list = {
        visible: { 
            opacity: 1,
            transition: {
                when: "beforeChildren",
                staggerChildren: 0.1,
            },
         },
        hidden: { 
            opacity: 0,
            transition: {
                when: "afterChildren",
            }
        },
      }
      
      const item = {
        visible: { opacity: 1, x: 0 },
        hidden: { opacity: 0, x: -100 },
      }

    return(
        <div className="content-container">
            <div className="content">
                <h2>Skills</h2>

                <InView>
                    {({ inView, ref }) => (
                        <MotionContainer ref={ref}>
                            <motion.ul initial="hidden" animate={inView ? "visible" : "hidden"} variants={list}>
                                <h4 className={styles.h4List}><motion.li variants={item}>SQL / TSQL</motion.li></h4>
                                <h4 className={styles.h4List}><motion.li variants={item}>SSIS</motion.li></h4>
                                <h4 className={styles.h4List}><motion.li variants={item}>SSRS</motion.li></h4>
                                <h4 className={styles.h4List}><motion.li variants={item}>Python</motion.li></h4>
                                <h4 className={styles.h4List}><motion.li variants={item}>C#</motion.li></h4>
                                <h4 className={styles.h4List}><motion.li variants={item}>Javascript / Typescript</motion.li></h4>
                                <h4 className={styles.h4List}><motion.li variants={item}>React</motion.li></h4>
                                <h4 className={styles.h4List}><motion.li variants={item}>Docker</motion.li></h4>
                            </motion.ul>
                        </MotionContainer>
                    )}
                </InView>
            </div>
        </div>
    )
}

export { Skills };